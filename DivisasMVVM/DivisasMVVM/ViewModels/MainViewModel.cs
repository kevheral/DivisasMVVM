﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DivisasMVVM.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Atributos

        private decimal dolars;

        private decimal pounds;

        private decimal euros;

        #endregion

        #region Propiedades
        public decimal pesos { get; set; }
        public decimal Dollars
        { set { if (dolars != value) { dolars = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pounds")); } } get { return dolars; } }


        public decimal Pounds
        { set { if (pounds != value) { pounds = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Pounds")); } } get { return pounds; } }

        public decimal Euros
        { set { if (euros != value) { euros = value; PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Euros")); } } get { return euros; } }

        public ICommand ConvertComand { get {return new RelayCommand(ConvertMoney); } }


        #endregion


        #region Eventos

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Commandos
        private async void ConvertMoney()
        {
           if (pesos <= 0)
            {
               await App.Current.MainPage.DisplayAlert("Error", "Debe ingresar un valor mayor que 0","Aceptar");
                return;
            }

            Dollars = pesos / 2889.222M;
            Euros = pesos / 2889.222M;
            Pounds = pesos / 2889.222M;

        #endregion

        }
    }
}
